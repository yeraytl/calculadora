def sumar(a, b):
    return a + b

def restar(a, b):
    return a - b

# Suma 1 y 2
resultado = sumar(1, 2)
print("El resultado de sumar 1 y 2 es:", resultado)

# Suma 3 y 4
resultado = sumar(3, 4)
print("El resultado de sumar 3 y 4 es:", resultado)

# Resta 5 de 6
resultado = restar(6, 5)
print("El resultado de restar 5 de 6 es:", resultado)

# Resta 7 de 8
resultado = restar(8, 7)
print("El resultado de restar 7 de 8 es:", resultado)

